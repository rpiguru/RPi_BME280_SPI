# Python Library of BME280 thru SPI connection.

## Connect to the Raspberry Pi


## Installation

- Enable SPI0 bus on RPi.
    
        sudo raspi-config
    
    Go to `5(Interfacing Options) -> P4(SPI)` and enable SPI bus, and reboot.

- Install dependencies

    After enabling SPI bus, we need to install some python packages.
    
        sudo pip install spidev


## Test

    python BME280.py
